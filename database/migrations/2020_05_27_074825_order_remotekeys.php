<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderRemotekeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_remotekeys', function (Blueprint $table) {
            $table->bigInteger('order_id')->index()->unsigned()->unique();
            $table->float('fprice', 16)->default(0);
            $table->string('cur')->nullable();
            $table->integer('project_id')->default(0);
            $table->float('discount', 16)->default(0);
            $table->string('period')->nullable();
            $table->string('licdata')->nullable();
            $table->string('renewmd5')->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_remotekeys');
    }
}
