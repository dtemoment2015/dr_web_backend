<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('hash', 32)->nullable()->unique();
            $table->string('session', 32)->nullable()->unique();
            $table->bigInteger('user_id')->index()->nullable()->unsigned();
            $table->bigInteger('state_id')->default(1)->unsigned();
            $table->string('preset_client_type')->nullable();
            $table->string('subscription_renewable')->nullable();
            $table->string('period')->nullable();
            $table->text('title');
            $table->text('subscription_product_name');
            $table->string('quickorder')->nullable();
            $table->string('letter')->nullable();
            $table->string('pdaorder')->nullable();
            $table->string('project')->nullable();
            $table->string('product')->nullable();
            $table->string('burncd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
