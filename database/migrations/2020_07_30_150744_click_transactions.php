<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClickTransactions extends Migration
{
    public function up()
    {
        Schema::create('click_transactions', function (Blueprint $table) {
        
            $table->id();
            $table->bigInteger('click_trans_id')->nullable();
            $table->bigInteger('service_id')->nullable();
            $table->bigInteger('click_paydoc_id')->nullable();
            $table->string('merchant_trans_id', 128)->nullable();
            $table->float('amount', 16, 2)->nullable()->default(0);
            $table->tinyInteger('action')->default(0);
            $table->tinyInteger('error')->default(0);
            // $table->string('error_note', 128)->nullable();
            $table->timestamp('sign_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('click_transactions');
    }
}
