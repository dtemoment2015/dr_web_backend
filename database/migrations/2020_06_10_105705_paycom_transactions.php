<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaycomTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paycom_transactions', function (Blueprint $table) {
            $table->string('id',32)->primary();
            $table->bigInteger('time')->nullable();
            $table->float('amount', 16, 2)->nullable()->default(0);
            $table->bigInteger('order_id')->index();
            $table->bigInteger('create_time')->nullable();
            $table->bigInteger('perform_time')->nullable();
            $table->bigInteger('cancel_time')->nullable();
            $table->tinyInteger('state')->nullable()->default(0);
            $table->tinyInteger('reason')->nullable();
            $table->text('receivers')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paycom_transactions');
    }
}
