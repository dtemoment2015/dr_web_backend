<?php

namespace App\Payments\Click;

use Illuminate\Database\Eloquent\Model;

class ClickTransaction extends Model
{

    protected $table = 'click_transactions';

    protected $appends = ['merchant_prepare_id', 'error_note'];

    protected $fillable = [
        'click_trans_id',
        'service_id',
        'click_paydoc_id',
        'merchant_trans_id',
        'amount',
        'action',
        'error',
        'sign_time'
    ];

    protected $hidden = [
        'amount',
        'action',
        'click_paydoc_id',
        'sign_time',
        'service_id',
        'created_at',
        'updated_at',
        'id'
    ];

    public function getMerchantPrepareIdAttribute()
    {
        return $this->attributes['id'];
    }

    public function getErrorNoteAttribute()
    {
        return "";
    }
}
