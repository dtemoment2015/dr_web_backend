<?php

namespace App\Payments\Paycom;


class PaycomRequest
{
    public $payload;

    public $id;

    public $method;

    public $params;
 
    public $amount;
 
    public function __construct()
    {
        $this->payload = \request()->all();
        if (!$this->payload) {
            $exception = new PaycomException(
                null,
                'Invalid JSON-RPC object.',
                PaycomException::ERROR_INVALID_JSON_RPC_OBJECT
            );
            $result  = $exception->send();
            header('Content-Type: application/json');
            echo json_encode($result->original);
            exit;
        }
        $this->id = isset($this->payload['id']) ? 1 * $this->payload['id'] : null;
        $this->method = isset($this->payload['method']) ? trim($this->payload['method']) : null;
        $this->params = isset($this->payload['params']) ? $this->payload['params'] : [];
        $this->amount = isset($this->payload['params']['amount']) ? 1 * $this->payload['params']['amount'] : null;
        $this->params['request_id'] = $this->id;
    }

    public function account($param)
    {
        return isset($this->params['account'], $this->params['account'][$param]) ? $this->params['account'][$param] : null;
    }
}