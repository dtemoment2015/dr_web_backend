<?php

namespace App\Http\Controllers;

use App\Http\Requests\PayRequest;
use App\Jobs\generatorKeyJob;
use App\Jobs\notifyOrderJob;
use App\Orders\Order;
use Illuminate\Http\Request;

class PayController extends Controller
{
    public function index(PayRequest $request)
    {
        $order = Order::findOrFail($request->id);


        notifyOrderJob::dispatch(
            $order,
            $request->payment
        );

        generatorKeyJob::dispatch(
            $order
        );



        return response()->json($order);
    }
}
