<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\Controller;
use App\Jobs\generatorKeyJob;
use App\Jobs\notifyOrderJob;
use App\Orders\Order;
use App\Payments\Click\ClickTransaction;
use Illuminate\Http\Request;

class ClickController extends Controller
{
    const ERROR_SUCCESS = 0;
    const ERROR_SIGN_CHECK = -1;
    const ERROR_AMOUNT = -2;
    const ERROR_ACTION = -3;
    const ERROR_ALREADY_PAID = -4;
    const ERROR_NOT_EXIST = -5;
    const ERROR_NOT_EXIST_TRANSACTION = -6;
    const ERROR_UPDATE = -7;
    const ERROR_CLICK = -8;
    const ERROR_TRANSACTION_CANCELLED = -9;
    const SECRET_KEY = 'K0hLYd8KzwSHS';

    public function index(Request $request)
    {
        if (optional($request)->action == 0) {
            return $this->prepare($request->all());
        } elseif (optional($request)->action == 1) {
            return $this->complete($request->all());
        }
        return response()->json([
            'error' => self::ERROR_ACTION,
            'error_note' => 'Запрашиваемое действие не найдено'
        ]);
    }

    public function prepare($params = [])
    {
        if (!$this->auth('prepare')){

        }
        $order = Order::whereId(optional($params)['merchant_trans_id'])
            ->payment()
            ->first();     
        if (!$order) {
            return response()->json([
                'error' => self::ERROR_NOT_EXIST,
                'error_note' => 'Не найдет пользователь/заказ (проверка параметра merchant_trans_id)'
            ]);
        } elseif ((float) optional($order->price)->discountsum != (float) optional($params)['amount']) {
            return response()->json([
                'error' => self::ERROR_AMOUNT,
                'error_note' => 'Неверная сумма оплаты'
            ]);
        }
        return response()->json(
            ClickTransaction::firstOrCreate(
                [
                    'click_trans_id' => optional($params)['click_trans_id']
                ],
                $params
            )
        );
    }

    public function complete($params = [])
    {

        if (!$this->auth('complete')){
            
        }

        $transaction  = ClickTransaction::whereClickTransId(
            optional($params)['click_trans_id']
        )
            ->whereId(optional($params)['merchant_prepare_id'])
            ->limit(1)
            ->first();

        if (!$transaction) {
            return response()->json([
                'error' => self::ERROR_TRANSACTION_CANCELLED,
                'error_note' => 'Transaction cancelled'
            ]);
        }

        $order = Order::whereId(optional($params)['merchant_trans_id'])
            ->payment()
            ->first();

        if (!$order) {
            return response()->json([
                'error' => self::ERROR_NOT_EXIST,
                'error_note' => 'Не найдет пользователь/заказ (проверка параметра merchant_trans_id)'
            ]);
        } elseif ((float) optional($order->price)->discountsum != (float) optional($params)['amount']) {
            return response()->json([
                'error' => self::ERROR_AMOUNT,
                'error_note' => 'Неверная сумма оплаты'
            ]);
        }
        $transaction->error = optional($params)['error'];
        $transaction->action = optional($params)['action'];
        $transaction->amount = optional($params)['amount'];
        $transaction->sign_time = optional($params)['sign_time'];
        $transaction->save();
        if ($transaction->error == 0) {
            $order = Order::whereId($transaction->merchant_trans_id)
                        ->limit(1)
                        ->first();

                    $order->state_id = 2;
                    $order->save();
                    generatorKeyJob::dispatch(
                        $order
                    );
                    notifyOrderJob::dispatch(
                        $order,'Click'
                    );
        }

        return response()->json(
            $transaction
        );
    }

    private function auth($type = 'prepare')
    {
        $check = NULL;

        if ($type == 'complete') {
            $check = md5(
                request('click_trans_id') .
                    request('service_id') .
                    self::SECRET_KEY .
                    request('merchant_trans_id') .
                    request('merchant_prepare_id') .
                    request('amount') .
                    request('action')  .
                    request('sign_time')
            );
        } elseif ($type == 'prepare') {
            $check = md5(
                request('click_trans_id') .
                    request('service_id') .
                    self::SECRET_KEY .
                    request('merchant_trans_id') .
                    request('amount') .
                    request('action')  .
                    request('sign_time')
            );
        }

        if (
            $check  &&  $check == request('sign_string')
        ) {
            return true;
        }

        return false;
    }
}
