<?php

namespace App\Http\Controllers\Payments;

use App\Payments\Paycom\PaycomResponse;
use App\Payments\Paycom\PaycomRequest;
use App\Payments\Paycom\PaycomTransaction;
use App\Payments\Paycom\PaycomException;
use App\Http\Controllers\Controller;
use App\Jobs\generatorKeyJob;
use App\Jobs\notifyOrderJob;
use App\Orders\Order;

class PaycomController extends Controller
{
    public $request;
    public $response;

    public function __construct()
    {
        $this->request = new PaycomRequest();
        $this->response = new PaycomResponse($this->request);
    }

    /**
     * Authorizes session and handles requests.
     */
    public function index()
    {
        try {
            switch (optional($this->request)->method) {
                case 'CheckPerformTransaction':
                    return $this->CheckPerformTransaction();
                    break;
                case 'CheckTransaction':
                    return $this->CheckTransaction();
                    break;
                case 'CreateTransaction':
                    return $this->CreateTransaction();
                    break;
                case 'PerformTransaction':
                    return $this->PerformTransaction();
                    break;
                case 'CancelTransaction':
                    return $this->CancelTransaction();
                    break;
                case 'GetStatement':
                    return $this->GetStatement();
                    break;
                default:
                    $this->response->error(
                        PaycomException::ERROR_METHOD_NOT_FOUND,
                        'Method not found.',
                        optional($this->request)->method
                    );
                    break;
            }
        } catch (PaycomException $exc) {
            return $exc->send();
        }
    }

    private function CheckPerformTransaction()
    {
        $order = Order::whereId(
            optional($this->request->params)['account']['order_id']
        )
            ->whereHas('state', function ($query) {
                $query->whereId(1)
                    ->whereId(1);
            })
            ->limit(1)
            ->first();

        $this->validateData($order);

        return $this->response->send(
            [
                'allow' => true
            ]
        );
    }

    private function CheckTransaction()
    {
        $transaction = PaycomTransaction::whereId(
            $this->request->params['id']
        )
            ->limit(1)
            ->first();

        if (!$transaction) {
            $this->response->error(
                PaycomException::ERROR_TRANSACTION_NOT_FOUND,
                'Transaction not found.'
            );
        }

        return $this->response->send($transaction);
    }

    private function CreateTransaction()
    {

        /** Информация о заказе */
        $order = Order::whereId(
            optional(
                $this->request->params
            )['account']['order_id']
        )->whereHas('state', function ($query) {
            $query->whereId(1);
        })
            ->limit(1)
            ->first();

        $this->validateData($order);

        if ($order->payment_process == 1) {
            $this->response->error(
                PaycomException::ERROR_INVALID_ACCOUNT,
                PaycomException::message(
                    'Блокируется паралельная транзация',
                    'Блокируется паралельная транзация',
                    'Блокируется паралельная транзация'
                ),
                'Sync'
            );
        }
        $order->payment_process = 1;
        $order->save();

        /** Транзакция данного заказа */
        $transaction =  $order->PaycomTransaction();

        /** Если существует уже транзакция с таким ID*/
        if ($existsTransaction = $transaction->limit(1)->first()) {
            if ($existsTransaction->create_time > (time() * 1000 + PaycomTransaction::TIMEOUT)) {
                /** Не прошло ли 24 часа после создания */
                $existsTransaction->state = -1;
                $existsTransaction->reason = 4;
                $existsTransaction->save();
                $this->response->error(
                    PaycomException::ERROR_COULD_NOT_PERFORM,
                    PaycomException::message(
                        'Время транзакции истекло',
                        'Время транзакции истекло',
                        'Время транзакции истекло'
                    ),
                    'Timout'
                );
            }
            /** Если странзакция уже провелась */
            elseif ($existsTransaction->state != 1) {
                $this->response->error(
                    PaycomException::ERROR_COULD_NOT_PERFORM,
                    PaycomException::message(
                        'Статус транзакции не соотвествующий',
                        'Статус транзакции не соотвествующий',
                        'Статус транзакции не соотвествующий'
                    ),
                    'State'
                );
            } elseif ($existsTransaction->id != optional($this->request->params)['id']) {
                $this->response->error(
                    PaycomException::ERROR_INVALID_ACCOUNT,
                    PaycomException::message(
                        'На данный заказ была уже создана транзанкиця',
                        'На данный заказ была уже создана транзанкиця',
                        'На данный заказ была уже создана транзанкиця'
                    ),
                    'State'
                );
            } else {
                $order->payment_process = 0;
                $order->save();
                return $this->response->send(
                    $existsTransaction
                );
            }
        }
        $order->payment_process = 0;
        $order->save();
        return $this->response->send(
            $order->PaycomTransaction()->create(
                [
                    'id' => optional($this->request->params)['id'],
                    'state' => PaycomTransaction::STATE_CREATED,
                    'amount' => $this->request->amount,
                    'receivers' => null,
                    'create_time' => (int) $this->request->params['time'],
                    'time' => (int) $this->request->params['time']
                ]
            )
        );
    }

    private function PerformTransaction()
    {
        $timeMS = time() * 1000;

        $transaction = PaycomTransaction::whereId(
            $this->request->params['id']
        )
            ->limit(1)
            ->first();

        if (!$transaction) $this->response->error(
            PaycomException::ERROR_TRANSACTION_NOT_FOUND,
            'Transaction not found.'
        );

        switch ($transaction->state) {
            case PaycomTransaction::STATE_CREATED:
                if (($transaction->create_time - $timeMS) > PaycomTransaction::TIMEOUT) {

                    $transaction->state = PaycomTransaction::REASON_CANCELLED_BY_TIMEOUT;

                    $transaction->save();

                    $this->response->error(
                        PaycomException::ERROR_COULD_NOT_PERFORM,
                        'Transaction is expired.'
                    );
                } else {

                    $order = Order::whereId($transaction->order_id)
                        ->limit(1)
                        ->first();

                    $order->state_id = 2;

                    $order->save();
                    info('before start job');
                    generatorKeyJob::dispatch(
                        $order
                    );
                    notifyOrderJob::dispatch(
                        $order,
                        'Paycom'
                    );

                    $transaction->state = PaycomTransaction::STATE_COMPLETED;
                    $transaction->perform_time = $timeMS;
                    $transaction->save();
                    return $this->response->send([
                        'transaction' => $transaction->id,
                        'perform_time' => $timeMS,
                        'state' => $transaction->state
                    ]);
                }
                break;
            case PaycomTransaction::STATE_COMPLETED:
                return $this->response->send([
                    'transaction' => $transaction->id,
                    'perform_time' => $transaction->perform_time,
                    'state' => $transaction->state
                ]);
                break;
            default:
                $this->response->error(
                    PaycomException::ERROR_COULD_NOT_PERFORM,
                    'Could not perform this operation.'
                );
                break;
        }
    }

    private function CancelTransaction()
    {
        $transaction = PaycomTransaction::whereId(
            optional($this->request->params)['id']
        )
            ->limit(1)
            ->first();

        if (!$transaction) $this->response->error(
            PaycomException::ERROR_TRANSACTION_NOT_FOUND,
            'Transaction not found.'
        );
        switch ($transaction->state) {

            case PaycomTransaction::STATE_CANCELLED:
                return $this->response->send([
                    'transaction' => $transaction->id,
                    'cancel_time' => $transaction->cancel_time,
                    'state' => $transaction->state
                ]);
                break;

            case PaycomTransaction::STATE_CANCELLED_AFTER_COMPLETE:
                return $this->response->send([
                    'transaction' => $transaction->id,
                    'cancel_time' => $transaction->cancel_time,
                    'state' => $transaction->state
                ]);
                break;

            case PaycomTransaction::STATE_CREATED:

                Order::whereId($transaction->order_id)
                    ->limit(1)
                    ->update(['state_id' => 3]);

                $transaction->cancel_time = time() *  1000;
                $transaction->state = -1;
                $transaction->reason = 3;
                $transaction->save();

                return $this->response->send([
                    'transaction' => $transaction->id,
                    'cancel_time' => $transaction->cancel_time,
                    'state' => $transaction->state
                ]);

                break;
            case PaycomTransaction::STATE_COMPLETED:

                Order::whereId($transaction->order_id)
                    ->limit(1)
                    ->update(['state_id' => 3]);

                $transaction->cancel_time = time() *  1000;
                $transaction->state = -2;
                $transaction->reason = 5;
                $transaction->save();

                return $this->response->send([
                    'transaction' => $transaction->id,
                    'cancel_time' => $transaction->cancel_time,
                    'state' => $transaction->state
                ]);

                break;
        }
    }

    private function GetStatement()
    {
        if (!isset($this->request->params['from'])) {
            $this->response->error(PaycomException::ERROR_INVALID_ACCOUNT, 'Incorrect period.', 'from');
        }

        if (!isset($this->request->params['to'])) {
            $this->response->error(PaycomException::ERROR_INVALID_ACCOUNT, 'Incorrect period.', 'to');
        }

        if (1 * $this->request->params['from'] >= 1 * $this->request->params['to']) {
            $this->response->error(PaycomException::ERROR_INVALID_ACCOUNT, 'Incorrect period. (from >= to)', 'from');
        }

        $transaction = new PaycomTransaction();
        $transactions = $transaction->report($this->request->params['from'], $this->request->params['to']);

        return $this->response->send(['transactions' => $transactions]);
    }

    private function validateData($order)
    {
        // Если цена не число.
        if (!is_numeric(optional($this->request->params)['amount'])) {
            throw new PaycomException(
                $this->request->id,
                'Incorrect amount.',
                PaycomException::ERROR_INVALID_AMOUNT
            );
        }
        if ($order) {
            // Если цена не соответсвует.
            if (round((int) optional($order->price)->discountsum * 100) != optional($this->request->params)['amount']) {
                throw new PaycomException(
                    $this->request->id,
                    'Incorrect amount.',
                    PaycomException::ERROR_INVALID_AMOUNT
                );
            }
        } else {
            // Если заказ не найден
            throw new PaycomException(
                $this->request->id,
                PaycomException::message(
                    'Неверный код заказа.',
                    'Harid kodida xatolik.',
                    'Incorrect order code.'
                ),
                PaycomException::ERROR_INVALID_ACCOUNT,
                'order_id'
            );
        }
    }
}
