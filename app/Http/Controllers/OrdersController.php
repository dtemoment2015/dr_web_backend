<?php

namespace App\Http\Controllers;

use App\Http\Requests\checkoutRequest;
use App\Jobs\generatorKeyJob;
use App\Jobs\notifyOrderJob;
use App\Mail\CreateOrderMail;
use App\Orders\Order;
use App\Orders\OrderUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class OrdersController extends Controller
{
    public function test()
    {
        $order = Order::whereId(
            request('__ID')
        )->limit(1)->firstOrFail();
      
        $order->state_id = 2;
        
        $order->save();

        generatorKeyJob::dispatch(
            $order
        );
        notifyOrderJob::dispatch(
            $order,
            'Avtech Pay'
        );
    }

    public function order()
    {
        return response()
            ->json(
                Order::whereHash(request('hash'))
                    ->limit(1)
                    ->firstOrFail()
            );
    }

    public function check(Request $request)
    {
        $body = $request->getContent();
        
        Cache::add(
            $hash = md5(time() . rand(1111111, 9111111) . $body),
            $body,
            86400 * 3
        );

        return response()->json([
            'order_url' => 'https://store.drweb.uz/basket?hash=' . $hash
        ]);
    }

    public function complete()
    {
        return response()->json(
            ['xml' => Cache::get(request('hash'))]
        );
    }

    public function checkout(checkoutRequest $request)
    {
        if (!$xmlString = Cache::get($request->cart_hash))
            return response()->json(
                ['errors' => [
                    'cart_hash' => [__('messages.cart_hash_error')]
                ]],
                422
            );

        $user = OrderUser::updateOrCreate(
            ['email' => $request->user['email']],
            $request->user
        );

        $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);

        $json = json_encode($xml);

        $input = json_decode($json, TRUE);

        $input['session'] = $request->session;

        $input['title'] = optional(optional($input)['title'])['ru'];

        $order = $user->order()->updateOrCreate(
            ['session' => $input['session']],
            $input
        );

        /** Запись всех объектов для заказа */
        $initOrder = $this
            ->initOrder($order, $input)
            ->load('state');

        /** Если первый раз, тогда отправляется на EMAIL */
        if ($order->wasRecentlyCreated) {
            Mail::to($initOrder->user)
                ->queue(
                    new CreateOrderMail(
                        $initOrder
                    )
                );
        }

        return response()->json($order);
    }

    protected function initOrder($order, $input)
    {
        /** Пользователь независимая моделька. Должен создаться перым, если его нет. */
        $order->state_id = 1;

        $order->hash = md5($order->id);

        $order->save();

        /** object mediapak **/

        if (optional($input)['mediapak']) {
            foreach (['price', 'certs'] as $field) {
                $order->mediapak->$field = optional($input['mediapak'])[$field];
            }
            $order->mediapak->save();
        }

        if (optional($input)['price']) {
            /** object price **/
            foreach (['scu', 'sum', 'discount', 'discountsum'] as $field) {
                $order->price->$field = optional($input['price'])[$field];
            }
            $order->price->save();
        }

        /** object remotekeys **/

        if (optional($input)['remotekeys']) {
            foreach (['fprice', 'cur', 'project_id', 'discount', 'period', 'licdata', 'renewmd5', 'product'] as $field) {
                $order->remotekeys->$field = optional($input['remotekeys'])[$field];
            }

            info($input);


            $order->remotekeys->save();
        }

        if (optional($input)['license']) {
            /** object licence **/
            foreach (['description', 'quantity'] as $field) {
                info(optional(optional($input['license'])['item'])[$field]);
                $order->licence->$field = optional(optional($input['license'])['item'])[$field];
            }
            $order->licence->save();
        }


        //Пользователи независимая таблица. Имеет только обратную связь.


        return $order;
    }
}
