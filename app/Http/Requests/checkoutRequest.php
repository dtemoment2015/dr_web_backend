<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class checkoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'session' => 'required',
            'cart_hash' => 'required',
            'user.name' => 'required',
            'user.last_name' => 'required',
            'user.email' => 'required|email',
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'session.required' => __('messages.session_error'),
            'cart_hash.required' => __('messages.cart_hash_error'),
            'user.name.required' => __('messages.user_name_error'),
            'user.email.required' => __('messages.user_email_error'),
            'user.email.email' => __('messages.user_email_error'),
            'user.last_name.required' => __('messages.user_last_name_error'),
        ];
    }
    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [];
    }
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()
            ->json(['errors' => $validator->errors()], 422));
    }
}
