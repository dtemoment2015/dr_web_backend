<?php

namespace App\Http\Middleware;

use App\Payments\Paycom\PaycomException;
use Closure;

class Paycom
{
    //qdWo&aB&EbPHRFc3nFgK9xpIK4a9aHJ&HqQF
    //%1KwFJCSs1TSj91@9G?wqIkRqi9iHPBSGpK3

    protected $config = [
        'key' => 'qdWo&aB&EbPHRFc3nFgK9xpIK4a9aHJ&HqQF',
        'login' => 'Paycom',
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //base64_encode($this->config['login'] . ":" . $this->config['key']);

        $headers['Authorization'] = request()->header('Authorization');
        if (
            !$headers || !isset($headers['Authorization']) ||
            !preg_match('/^\s*Basic\s+(\S+)\s*$/i', $headers['Authorization'], $matches) ||
            base64_decode($matches[1]) != $this->config['login'] . ":" . $this->config['key']
        ) {
            $exception =  new PaycomException(
                request('id'),
                'Insufficient privilege to perform this method. (Auth)',
                PaycomException::ERROR_INSUFFICIENT_PRIVILEGE
            );
            $result  = $exception->send();
            header('Content-Type: application/json');
            echo json_encode($result->original);
            exit;
        }

        return $next($request);
    }
}
