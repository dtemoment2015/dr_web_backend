<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderLicence extends Model
{
    protected $table = 'order_licencies';

    protected $hidden = ['order_id'];

    protected $primaryKey   = 'order_id';
    
    public $timestamps = false;

    protected $fillable = [
        'description',
        'quantity'
    ];
}
