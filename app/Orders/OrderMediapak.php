<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderMediapak extends Model
{
    protected $hidden = [
        'order_id'
    ];
    public $timestamps = false;

    protected $primaryKey   = 'order_id';

    protected $fillable = [
        'price',
        'certs'
    ];
}
