<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderRemotekey extends Model
{
    protected $hidden = ['order_id'];


    protected $primaryKey   = 'order_id';
    
    public $timestamps = false;

    protected $fillable = [
        'fprice',
        'cur',
        'project_id',
        'discount',
        'period',
        'licdata',
        'renewmd5',
        'product'
    ];
}
