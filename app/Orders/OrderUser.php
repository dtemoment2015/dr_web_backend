<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderUser extends Model
{
   public $timestamps = false;

   protected $fillable = [
      'name',
      'last_name',
      'phone',
      'email'
   ];

   public function order()
   {
       return $this->hasOne(
           'App\Orders\Order',
           'user_id'
       );
   }
   
}
