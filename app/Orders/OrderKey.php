<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderKey extends Model
{
    public $timestamps = false;

    protected $primaryKey   = 'order_id';
    
    protected $fillable = [
        'serial_number',
    ];
}
