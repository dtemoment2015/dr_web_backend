<?php

namespace App\Orders;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    
    protected $fillable = [
        'preset_client_type',
        'subscription_renewable',
        'period',
        'title',
        'subscription_product_name',
        'quickorder',
        'letter',
        'pdaorder',
        'project',
        'burncd',
        'session',
        'payment_process'
    ];

    protected $appends = ['payment'];

    protected $with = [
        'mediapak',
        'price',
        'remotekeys',
        'licence',
        'user',
        'state',
    ];

    public function mediapak()
    {
        return $this->hasOne(
            'App\Orders\OrderMediapak'
        )->withDefault(
            [
                'price' => null,
                'certs' => null
            ]
        );
    }

    public function price()
    {
        return $this->hasOne(
            'App\Orders\OrderPrice'
        )->withDefault(
            [
                'scu' => null,
                'sum' => null,
                'discount' => null,
                'discountsum' => null,
            ]
        );
    }

    public function remotekeys()
    {
        return $this->hasOne(
            'App\Orders\OrderRemotekey'
        )->withDefault(
            [
                'fprice' => null,
                'cur' => null,
                'project_id' => null,
                'discount' => null,
                'period' => null,
                'licdata' => null,
                'renewmd5' => null,
            ]
        );
    }

    public function licence()
    {
        return $this->hasOne(
            'App\Orders\OrderLicence'
        )->withDefault(
            [
                'description' => null,
                'quantity' => null,
            ]
        );
    }

    public function user()
    {
        return $this->belongsTo(
            'App\Orders\OrderUser'
        );
    }


    public function key()
    {
        return $this->hasOne(
            'App\Orders\OrderKey'
        )->withDefault(
            []
        );
    }

    public function state()
    {
        return $this->belongsTo(
            'App\State'
        );
    }

    public function PaycomTransaction()
    {
        return $this->hasOne(
            'App\Payments\Paycom\PaycomTransaction'
        );
    }

    public function scopePayment($query)
    {
        $query
            ->limit(1)
            ->whereHas('state', function ($queryState) {
                $queryState->whereId(1)
                    ->whereId(1);
            });
    }

    public function getPaymentAttribute()
    {
        if ($this->relationLoaded('price')) {
            return [

                'click' => 'https://my.click.uz/services/pay?service_id=15241&merchant_id=10833&amount=' . (int) round($this->price['discountsum']) . '&transaction_param=' . $this->attributes['id'],

                'payme' => 'https://checkout.paycom.uz/' . base64_encode(
                    'm=5f210b20a8d4d98c2aeb2890;ac.order_id=' . $this->attributes['id'] . ';a=' . ((int) round($this->price['discountsum']) * 100) . ';l=ru;ct=500;cr=UZS'
                )
            ];
        }
    }
}
