<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;


class notifyOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order = null;
    public $payment = '';
    public $message = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order = null, $payment = '')
    {
        $this->order = $order;
        $this->payment = $payment;

        $this->message  = '<b>Заказ #order_' . $this->order->id . '</b>

Дата: ' . $this->order->created_at . '        

Продукт: ' . $this->order->title . '

Метод оплаты: ' . $this->payment . '

Оплатили: ' . $this->order->remotekeys->fprice . '

Заказчик:
' . $this->order->user->name . '
' . $this->order->user->email . '
' . $this->order->user->phone . '

Ссылка на заказ:
https://store.drweb.uz/order/' . $this->order->hash;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $clientHttp = new Client();
        try {
            $clientHttp->get(
                'https://api.telegram.org/bot1473965400:AAHvM16SNeeIk5myCb0v80hTnUPWKeGfE60/sendMessage',
                [
                    'query' => [
                        'parse_mode' => 'html',
                        'disable_web_page_preview' => true,
                        'chat_id' => -465399787,
                        'text' => $this->message
                    ]
                ]
            );
            return true;
        } catch (ClientException $e) {
            return null;
        }
    }
}
