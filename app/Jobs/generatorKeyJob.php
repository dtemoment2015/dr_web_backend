<?php

namespace App\Jobs;

use App\Mail\PaidMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Mail;
use Mtownsend\XmlToArray\XmlToArray;

class generatorKeyJob implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $order = null;

    public function __construct($order = null)
    {
        $this->order = $order;
    }

    public function handle()
    {
        info('start job');

        $client = new Client();

        $query = [

            'login' => 'avt_19eb',
            'pwd' => '7d4c6b',
            'format' => "xml",
            // 'testing' => 'yes', //DEV
            'act' => 'serial',
            'computers' =>  $this->order->licence->quantity,
            'order_item_id' =>  $this->order->id,
            'amount' =>  1,
            'period' =>  $this->order->remotekeys->period,
            'product' =>  $this->order->remotekeys->product,
            'fprice' =>  $this->order->remotekeys->fprice,
            'cur' =>  $this->order->remotekeys->cur,
            'project_id' =>  $this->order->remotekeys->project_id,
            'discount' =>  $this->order->remotekeys->discount,
            'renewmd5' =>  $this->order->remotekeys->renewmd5,
            'licdata' =>  $this->order->remotekeys->licdata,

        ];

        // foreach ($query as &$q){
        //     $q = urlencode($q);
        // }

        // info($query);

        try {
            $content = $client->request(
                'GET',
                'https://f2.drweb.com/remotekeys',
                [
                    'query' => $query
                ]
            )
                ->getBody()
                ->getContents();

            info('CONTENT: ');
            info($content);

            if ($content = XmlToArray::convert($content)) {
                if (
                    $this->order->serial  = optional(optional($content)['serials'])['item']
                ) {
                    Mail::to($this->order->user)
                        ->send(
                            new PaidMail(
                                $this->order
                            )
                        );

                    info('start job SEND MESSAGW');
                }
            }
        } catch (RequestException $e) {
            // info($e);
            $clientHttp = new Client();
            try {
                $clientHttp->get(
                    'https://api.telegram.org/bot1473965400:AAHvM16SNeeIk5myCb0v80hTnUPWKeGfE60/sendMessage',
                    [
                        'query' => [
                            'parse_mode' => 'html',
                            'disable_web_page_preview' => true,
                            'chat_id' => -465399787,
                            'text' => '#ERROR_' . $this->order->id . ' - Генерация не прошла ' . PHP_EOL . $e->getMessage()
                        ]
                    ]
                );
                return true;
            } catch (RequestException $e) {
                info($e);
            }
        }
    }
}
