<?php

use Illuminate\Support\Facades\Route;

Route::post('checkout', 'OrdersController@checkout');
Route::get('order', 'OrdersController@order');

Route::post('test', 'OrdersController@test');

Route::post('check', 'OrdersController@check');

Route::post('pay', 'PayController@index');

Route::post('check/complete', 'OrdersController@complete');

Route::group(['prefix' => 'payments'], function () {
   Route::post('paycom', 'Payments\PaycomController@index')->middleware('paycom');
   Route::post('click', 'Payments\ClickController@index');
});
