<?php

return [
    'session_error' => 'Ошибка клиента, обновите страницу',
    'cart_hash_error' => 'Данные корзины устарели. Добавьте продукцию в корзину снова',
    'user_name_error' => 'Имя обязательное поле',
    'user_last_name_error' => 'Фамилия обязательное поле',
    'user_email_error' => 'Email обязательное поле',
];
