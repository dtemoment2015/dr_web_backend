<!DOCTYPE html>

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>DrWEB</title>
</head>

<body>

   <table border="0" cellpadding="0" cellspacing="0">
      <tbody>
         <tr>
            <td style="background:white;padding:0cm">
               <div align="center">
                  <table border="0" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td valign="top" style="padding:11.25pt 0cm 11.25pt 0cm">
                              <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><a href="https://estore.drweb.kz/" target="_blank" rel=" noopener noreferrer"><span style="text-decoration:none"><img src="https://img.allsoft.ru/market_resources/104903/logo/logo.png" border="0" height="64" width="234"></span></a></p>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </td>
         </tr>
         <tr>
            <td style="background:white;padding:11.25pt">
               <div align="center">
                  <table border="0" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td style="padding:0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td style="padding:7.5pt 0cm 0cm 0cm">
                                          <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:24pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:24pt">Здравствуйте,</span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:0cm">
                                          <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:24pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:24pt">{{$order->user->name}}.</span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:15pt 0cm 0cm 0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">Благодарим Вас за приобретение программы <strong>{{$order->title}}</strong>.</span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:7.5pt 0cm 0cm 0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><strong><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">Ваш серийный номер:<br>{{$order->serial}}</span>
                                             </strong><br><br><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">При покупке нескольких лицензий Вы получите 1 многопользовательский ключ на несколько рабочих станций.<br><br><strong><span style="font-family:'helvetica' , sans-serif">Если программа еще не установлена, то:</span></strong><br><br>Скачайте дистрибутив программы с нашего сайта <a href="http://download.drweb.com/" target="_blank" rel=" noopener noreferrer">http://download.drweb.com/</a> и запустите установочный файл, в процессе установки получите ключевой файл, указав серийный номер и регистрационные данные.<br><br><strong><span style="font-family:'helvetica' , sans-serif">Для активации продления:</span></strong><br><br>Щелкните по значку антивируса в области уведомлений и выберите пункт «Лицензия». В открывшемся окне нажмите «Купить или активировать новую лицензию». Далее укажите серийный номер и свои регистрационные данные.<br><br>Если Вам необходимо, то Вы можете сформировать лицензионный сертификат:<br><a href="http://products.drweb.com/register/certificate/" target="_blank" rel=" noopener noreferrer">http://products.drweb.com/register/certificate/</a><br><br>Мы искренне надеемся, что Вы останетесь довольны результатами работы нашей программы и станете нашим постоянным пользователем!<br>Для установки мобильной версии (если приобретена программа Dr.Web Security Space) необходимо ознакомиться с инструкцией, по ссылке: <a href="http://download.geo.drweb.com/pub/drweb/android/pro/HTML/ru/" target="_blank" rel=" noopener noreferrer">http://download.geo.drweb.com/pub/drweb/android/pro/HTML/ru/</a></span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:15pt 0cm 3.75pt 0cm">
                                          <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><strong><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">Информация о заказе </span></strong></p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center">&nbsp;</p>
               <div align="center">
                  <table border="0" cellpadding="0" cellspacing="0" style="max-width:375pt">
                     <tbody>
                        <tr>
                           <td style="border-style:dashed none none none;border-top-color:#aaaaaa;border-width:1pt medium medium medium;padding:7.5pt 0cm 0cm 0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding:0cm">
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><strong><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">Номер заказа &nbsp;</span></strong></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">{{$order->id}}</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="border-style:solid none none none;border-top-color:#eaeaea;border-width:1pt medium medium medium;padding:7.5pt 0cm 0cm 0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding:0cm">
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">Дата заказа &nbsp;</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">{{$order->created_at}}</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>

                        <tr>
                           <td style="border-style:solid none none none;border-top-color:#eaeaea;border-width:1pt medium medium medium;padding:7.5pt 0cm 0cm 0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding:0cm">
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">E-mail &nbsp;</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt"><a target="_blank" rel=" noopener noreferrer"><span style="color:#64ad15">{{$order->user->email}}</span></a></span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="border-style:solid none none none;border-top-color:#eaeaea;border-width:1pt medium medium medium;padding:7.5pt 0cm 0cm 0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding:0cm">
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">Название программы &nbsp;</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:12pt">
                                                                        {{$order->title}} </span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="border-bottom-color:#aaaaaa;border-style:dashed none dashed none;border-top-color:#eaeaea;border-width:1pt medium 1pt medium;padding:7.5pt 0cm 0cm 0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td valign="top" style="padding:0cm">
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><strong><span style="color:#2d3138;font-family:'helvetica' , sans-serif;font-size:12pt">Сумма заказа по данной позиции &nbsp;</span></strong></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><strong><span style="color:#2d3138;font-family:'helvetica' , sans-serif;font-size:12pt">{{$order->remotekeys->fprice}} {{$order->remotekeys->cur}}</span></strong></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center">&nbsp;</p>
               <div align="center">
                  <table border="0" cellpadding="0" cellspacing="0" width="500" style="width:375pt">
                     <tbody>
                        <tr>
                           <td style="padding:0cm">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                 <tbody>
                                    <tr>
                                       <td style="padding:22.5pt 0cm 0cm 0cm">
                                          <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="color:#333333;font-family:'helvetica' , sans-serif;font-size:24pt">Остались вопросы?</span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:15pt 0cm 0cm 0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">Если вы хотите узнать больше об оплате, условиях поставки или по любому другому вопросу, обратитесь в наш Центр поддержки: <a target="_blank" rel=" noopener noreferrer"><span lang="EN-US" style="color:#64ad15;font-family:'calibri' , sans-serif">drweb@avtech.uz</span></a></span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:15pt 0cm 0cm 0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">Мы отвечаем на запросы по эл. почте в течение одного рабочего дня!</span></p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:15pt 0cm 0cm 0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">Или по телефонам:</span><br>+998 97 7149327</p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="padding:15pt 0cm 0cm 0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:#666666;font-family:'helvetica' , sans-serif;font-size:12pt">При обращении в наш Центр поддержки, указывайте свой номер заказа <strong><span style="font-family:'helvetica' , sans-serif">{{$order->id}}</span></strong>, чтобы мы могли обслужить вас в более короткий срок.</span></p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center">&nbsp;</p>
            </td>
         </tr>
      </tbody>
   </table>


</body>

</html>