<!DOCTYPE html>

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>DrWEB</title>
</head>

<body>

   <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
      <tbody>
         <tr>
            <td style="background:white;padding:0cm">
               <div align="center">
                  <table border="0" cellpadding="0" cellspacing="0" width="700" style="width:525pt">
                     <tbody>
                        <tr>
                           <td valign="top" width="580" style="border-left-color:#b2b2b2;border-style:solid none none solid;border-top-color:white;border-width:1pt medium medium 1pt;padding:0cm;width:435pt">
                              <table border="1" cellpadding="0" cellspacing="0" width="580" style="border-style:solid none none none;border-top-color:#b2b2b2;border-width:1pt medium medium medium;width:435pt">
                                 <tbody>
                                    <tr style="height:16.5pt">
                                       <td colspan="3" style="border:medium;height:16.5pt;padding:0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm">&nbsp;</p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td width="22" style="border:medium;padding:0cm;width:16.5pt">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm">&nbsp;</p>
                                       </td>
                                       <td valign="top" width="536" style="border:medium;padding:0cm;width:402pt">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><img alt="Dr.Web" height="39" src="https://api2.rrpo.uz/images/image001.jpg" width="168"><br>&nbsp;</p>
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:7.5pt 0cm 0cm 0cm">
                                                                  <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:24pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:24pt">Здравствуйте,</span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 0cm 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">Благодарим вас за заказ в нашем интернет-магазине <a href="https://estore.drweb.uz" target="_blank" rel=" noopener noreferrer"><span style="color:#64ad15">Dr.Web UZ</span></a>. Ваш заказ оформлен и ожидает оплаты.</span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 7.5pt 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">Ниже вы сможете найти подробную информацию о вашем заказе.</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="font-family:'tahoma' , sans-serif">&nbsp;</span></p>
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:7.5pt 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">&nbsp;</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="border-style:dashed none none none;border-top-color:#aaaaaa;border-width:1pt medium medium medium;padding:7.5pt 0cm 7.5pt 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><strong><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Информация о заказе</span></strong></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Номер заказа: &nbsp; </span><span lang="EN-US" style="color:black;font-family:'arial' , sans-serif;font-size:12pt">{{$order->id}}</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Дата создания: &nbsp; {{$order->updated_at}}</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td style="border-bottom-color:#aaaaaa;border-style:none none dashed none;border-width:medium medium 1pt medium;padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span lang="EN-US" style="color:black;font-family:'arial' , sans-serif;font-size:12pt">E-mail: &nbsp; </span><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">&nbsp;{{$order->user->email}}</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="border-bottom-color:#aaaaaa;border-style:none none dashed none;border-width:medium medium 1pt medium;padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span lang="EN-US" style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Контактный номер: &nbsp; </span><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">&nbsp;{{$order->user->phone}}</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding:7.5pt 0cm 7.5pt 0cm">
                                                      <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span lang="EN-US">&nbsp;</span></p>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span lang="EN-US" style="font-family:'tahoma' , sans-serif">&nbsp;</span></p>
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm">
                                                      <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span lang="EN-US">&nbsp;</span></p>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="87%" style="width:87%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><strong><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">
                                                                                                   {{$order->title}}
                                                                                                </span></strong></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                                  <table align="right" border="0" cellpadding="0" cellspacing="0" width="10%" style="width:10%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">&nbsp;</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="10%" style="width:10%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">&nbsp;</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                                  <table align="right" border="0" cellpadding="0" cellspacing="0" width="87%" style="width:87%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">&nbsp;</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="border-style:solid none none none;border-top-color:#eaeaea;border-width:1pt medium medium medium;padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Доставка &nbsp;</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                                  <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">на E-mail</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="border-style:solid none none none;border-top-color:#eaeaea;border-width:1pt medium medium medium;padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Количество &nbsp;</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                                  <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">{{$order->licence->quantity}}</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                               
                                                <tr>
                                                   <td style="padding:0cm">
                                                      <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm">&nbsp;</p>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="border-bottom-color:#eaeaea;border-style:solid none dashed none;border-top-color:#eaeaea;border-width:1pt medium 1pt medium;padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">Итого &nbsp;</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                                  <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><span style="color:black;font-family:'arial' , sans-serif;font-size:12pt">{{$order->remotekeys->fprice}} {{$order->remotekeys->cur}}</span></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding:0cm">
                                                      <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm">&nbsp;</p>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="border-bottom-color:#aaaaaa;border-style:none none dashed none;border-width:medium medium 1pt medium;padding:7.5pt 0cm 0cm 0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td valign="top" style="padding:0cm">
                                                                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm 0cm 7.5pt 0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><strong><span style="color:#2d3138;font-family:'arial' , sans-serif;font-size:12pt">Сумма к оплате &nbsp;</span></strong></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                                  <table align="right" border="0" cellpadding="0" cellspacing="0" width="47%" style="width:47%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:0cm">
                                                                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="padding:0cm">
                                                                                          <p align="right" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:right"><strong><span style="color:#2d3138;font-family:'arial' , sans-serif;font-size:12pt">{{$order->remotekeys->fprice}} {{$order->remotekeys->cur}}</span></strong></p>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><span style="font-family:'tahoma' , sans-serif">&nbsp;</span></p>
                                          <table border="0" cellpadding="0" cellspacing="0" width="500" style="width:375pt">
                                             <tbody>
                                                <tr>
                                                   <td style="padding:0cm">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                         <tbody>
                                                            <tr>
                                                               <td style="padding:0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">Чтобы перейти на страницу оплаты заказа, нажмите на кнопку ниже:</span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="border-radius:0px;padding:0cm">
                                                                  <div align="center">
                                                                     <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%">
                                                                        <tbody>
                                                                           <tr>
                                                                              <td style="border-radius:0px;padding:18.75pt 0cm 0cm 0cm">
                                                                                 <div align="center">
                                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                                       <tbody>
                                                                                          <tr>
                                                                                             <td style="background:#9bc225;padding:0cm">
                                                                                                <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><a href="https://store.drweb.uz/order/{{$order->hash}}" target="_blank" rel=" noopener noreferrer"><span style="border:1pt solid #9bc225;color:white;font-family:'tahoma' , sans-serif;font-size:12pt;padding:11pt;text-decoration:none">ОПЛАТИТЬ ЗАКАЗ</span></a></p>
                                                                                             </td>
                                                                                          </tr>
                                                                                       </tbody>
                                                                                    </table>
                                                                                 </div>
                                                                              </td>
                                                                           </tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 0cm 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">После подтверждения оплаты на вашу электронную почту будет отправлено письмо, содержащее файлы лицензирования или коды активации.</span></p>
                                                               </td>
                                                            </tr>
                                                       
                                                            <tr>
                                                               <td style="padding:22.5pt 0cm 0cm 0cm">
                                                                  <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:24pt">Остались вопросы?</span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 0cm 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">Если вы хотите узнать больше об оплате, условиях поставки или по любому другому вопросу, обратитесь в наш Центр поддержки: <a href="mailto:drweb@avtech.uz" target="_blank" rel=" noopener noreferrer">
                                                                                            drweb@avtech.uz          

                                                                  </a></span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 0cm 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">Мы отвечаем на запросы по эл. почте в течение одного рабочего дня!</span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 0cm 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">Или по телефонам:<br>+998 97 7149327</span></p>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td style="padding:15pt 0cm 0cm 0cm">
                                                                  <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;line-height:18.75pt;margin:0cm 0cm 0.0001pt 0cm"><span style="color:black;font-family:'tahoma' , sans-serif;font-size:12pt">При обращении в наш Центр поддержки, указывайте свой номер заказа <strong><span style="font-family:'tahoma' , sans-serif">{{$order->id}}</span></strong>, чтобы мы могли обслужить вас в более короткий срок.</span></p>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><br>&nbsp;</p>
                                       </td>
                                       <td width="22" style="border:medium;padding:0cm;width:16.5pt">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm">&nbsp;</p>
                                       </td>
                                    </tr>
                                    <tr style="height:16.5pt">
                                       <td colspan="3" style="border:medium;height:16.5pt;padding:0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm">&nbsp;</p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td rowspan="2" valign="top" width="126" style="background:#7dbd21;border-style:solid none none none;border-top-color:white;border-width:1pt medium medium medium;padding:0cm;width:94.5pt">
                              <table border="0" cellpadding="0" cellspacing="0" width="126" style="width:94.5pt">
                                 <tbody>
                                    <tr style="height:16.5pt">
                                       <td style="height:16.5pt;padding:0cm">
                                          <p style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm"><img src="https://img.allsoft.ru/allsoftru/img/allsoft/media/for_mail/drweb_bg_right_ru.png" border="0" height="248"  width="126"></p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td valign="top" width="100%" style="background:#f3f6f0;padding:0cm;width:100%">
                              <table border="1" cellpadding="0" cellspacing="0" style="border-style:solid none none none;border-top-color:#f3f6f0;border-width:4.5pt medium medium medium">
                                 <tbody>
                                    <tr>
                                       <td valign="top" width="150" style="border:medium;padding:0cm;width:112.5pt">
                                          <p align="center" style="font-family:'calibri' , 'sans-serif';font-size:11pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><img alt="Dr.Web" border="0" height="28"  src="https://api2.rrpo.uz/images/image003.png"  width="41"></p>
                                          <p align="center" style="font-family:'times new roman' , 'serif';font-size:12pt;margin:0cm 0cm 0.0001pt 0cm;text-align:center"><span style="font-size:8.5pt"><a href="http://www.drweb.uz" target="_blank" rel=" noopener noreferrer"><span style="color:#7abd19;font-family:'tahoma' , sans-serif">«Доктор&nbsp;Веб»<br>2003—2020</span></a></span></p>
                                       </td>
                                       <td style="border:medium;padding:0cm">
                                          <p style="font-family:'times new roman' , 'serif';font-size:12pt;margin-bottom:12pt;margin-left:0cm;margin-right:12pt"><span style="font-family:'tahoma' , sans-serif;font-size:8.5pt">«Доктор Веб» — российский производитель антивирусных средств защиты информации под маркой Dr.Web. Продукты Dr.Web разрабатываются с 1992 года. Компания — ключевой игрок на российском рынке программных средств обеспечения базовой потребности бизнеса — безопасности информации.</span></p>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </td>
         </tr>
      </tbody>
   </table>


</body>

</html>