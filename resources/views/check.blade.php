<html>
<body onload="document.createElement('form').submit.call(document.getElementById('myForm'))">
    <h3>Redirect...</h3>
    <form id="myForm" name="myForm" action="https://store.drweb.uz/basket" method="post">
        <input type="hidden" name="xml" value="{{$xml}}" />
        <noscript>
            <h1>У вас не поддерживается Javascript. Нажмите "далее", чтобы продолжить</h1>
            <button type="submit">Далее</button>
        </noscript>
    </form>
</body>
</html>